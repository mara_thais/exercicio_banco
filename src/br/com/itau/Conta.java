package br.com.itau;

public class Conta {
    private int agencia;
    private int numero;
    private int id_cliente;
    private double saldo;

    public boolean depositar(double valor){
        if(valor>0){
            this.saldo=this.saldo+valor;
            return(true);
        }else{
            return(false);}
    }

    public boolean sacar(double valor) {
        if (this.saldo >= valor) {
            this.saldo -= valor;
            return (true);
        } else {
            return (false);
        }
    }

    public Conta(int agencia, int numero, int id_cliente, double saldo) {
        this.agencia = agencia;
        this.numero = numero;
        this.id_cliente = id_cliente;
        this.saldo = saldo;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
