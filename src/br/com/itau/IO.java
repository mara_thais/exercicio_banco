package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {
    public static Map<String,String> solicitarDados(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite o Nome: ");
        String nome = scanner.nextLine();

        System.out.println("Digite o CPF: ");
        String cpf = scanner.nextLine();

        System.out.println("Digite o Endereço: ");
        String endereco = scanner.nextLine();

        System.out.println("Digite a Idade: ");
        String idade = scanner.nextLine();


        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);
        dados.put("cpf", cpf);
        dados.put("endereco", endereco);
        dados.put("idade", idade);

        return dados;

    }
}
