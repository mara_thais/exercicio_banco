package br.com.itau;

import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static  void main(String[] args) {
        Map<String, String> dados = IO.solicitarDados();
        Scanner in = new Scanner(System.in);
        //instancia do objeto 1
        Cliente cliente = new Cliente(
                dados.get("nome"),
                dados.get("cpf"),
                dados.get("endereco"),
                Integer.parseInt(dados.get("idade"))
        );

        if (cliente.verificaidade() == true){
            Random numero = new Random();
            int numeroGerado = numero.nextInt(100);
            Conta conta = new Conta(
                    999,
                    999,
                    numeroGerado,
                    2000
            );
            System.out.println("Cliente " + cliente.getNome() +" cadastrado com sucesso!");
            System.out.println(numeroGerado);
            System.out.println(cliente.getNome());
            System.out.println(conta.getSaldo());

            System.out.println("Digite o valor a ser depositado: ");
            double valor_dep = in.nextDouble();
            boolean deposito_efetuado = conta.depositar(valor_dep);
            System.out.println(conta.getSaldo());

            System.out.println("Digite o valor a ser sacado: ");
            double valor_saq = in.nextDouble();
            boolean saque_efetuado = conta.sacar(valor_saq);
            System.out.println(conta.getSaldo());
        }
        else{
            System.out.println("Cliente " + cliente.getNome() + " não possui idade("  + cliente.getIdade() +  ") permitida!");
        }

//        System.out.println(cliente.getId_cliente());
//        System.out.println(cliente.getNome());
//        System.out.println(cliente.getCpf());
//        System.out.println(cliente.getEndereco());
//        System.out.println(cliente.getIdade());

    }
}
